***Settings***
Documentation   Login Tentativa
Resource  ../../resources/base.robot

Test Template     Tentativa de login
Suite Setup       Start Session
Suite Teardown    Finish Session
Test Teardown     Finish Test

***Test Cases***
Senha incorreta            admin@zepalheta.com.br    abc123     Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco            joao@gmail.com.br         ${EMPTY}   O campo senha é obrigatório!
Email em branco            ${EMPTY}                  123456     O campo email é obrigatório!
Email e senha em branco    ${EMPTY}                  ${EMPTY}   Os campos email e senha não foram preenchidos!

***Keywords***
Tentativa de login
    [Arguments]   ${input_email}    ${input_senha}    ${output_mensagem}
    Acesso a página Login
    Submeto minhas credenciais    ${input_email}    ${input_senha}
    Devo ver um toaster com a mensagem    ${output_mensagem}