***Settings***
Documentation    Cadastro de equipamentos

Resource    ../../resources/base.robot

Suite Setup       Login Session
Suite Teardown    Finish Session
Test Teardown     Finish Test

***Test Cases***
Novo Equipamento
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...    Guitarra Fender       52
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação:    Equipo cadastrado com sucesso!

Equipamento Duplicado
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...    Violino Stradivarus   100
    Mas esse equipamento já existe no sistema
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação de erro:    Erro na criação de um equipo

Campos Obrigatórios
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...    ${EMPTY}   ${EMPTY}
    Quando faço a inclusão desse equipamento
    Então devo ver mensagens informando que os campos do cadastro de equipamentos são obrigatórios

Nome é Obrigatório
    [Template]    Validação de Campos
    ${EMPTY}           50          Nome do equipo é obrigatório

Valor é Obrigatório
    [Template]         Validação de Campos
    Guitarra Fender    ${EMPTY}    Diária do equipo é obrigatória
    
***Keywords***
Validação de Campos
    [Arguments]    ${nome}    ${valor}    ${saída}

    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...    ${nome}    ${valor}
    Quando faço a inclusão desse equipamento
    Então devo ver o texto:     ${saída}