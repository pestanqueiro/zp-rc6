***Settings***
Documentation    Camada de serviços do projeto de automação

Resource    helpers.robot
Library     RequestsLibrary
Library     Collections

***Variables***
${base_api_url}=    http://zepalheta-api:3333

***Keywords***
## Helpers
Get Session Token
    ${resp}=     Post Session    admin@zepalheta.com.br    pwd123
    ${token}=    Convert To String    Bearer ${resp.json()['token']}
    [Return]     ${token}

# POST /sessions
Post Session
    [Arguments]       ${email}             ${password}
    Create Session    zp-api               ${base_api_url}
    &{payload}=       Create Dictionary    email=${email}    password=${password}
    ${resp}=          Post request         zp-api    /sessions    json=${payload}
    [Return]          ${resp}

# POST /customers
Post Customer
    [Arguments]         ${payload}
    Create Session      zp-api                      ${base_api_url}
    ${token}=           Get Session Token
    &{headers}=         Create Dictionary           content-type=application/json      authorization=${token}
    ${resp}=            Post Request                zp-api       /customers            data=${payload}    headers=${headers}
    [Return]            ${resp}

# PUT /customers
Put Customer
    [Arguments]         ${payload}    ${user_id}
    Create Session      zp-api                      ${base_api_url}
    ${token}=           Get Session Token
    &{headers}=         Create Dictionary           content-type=application/json      authorization=${token}
    ${resp}=            Put Request                 zp-api       /customers/${user_id}            data=${payload}    headers=${headers}
    [Return]            ${resp}

# GET /customers
Get Customers
    Create Session      zp-api                           ${base_api_url}
    ${token}=           Get Session Token
    &{headers}=         Create Dictionary                content-type=application/json    authorization=${token}
    ${resp}=            Get Request                      zp-api    /customers    headers=${headers}
    [Return]            ${resp}

Get Unique Customer
    [Arguments]         ${user_id}
    Create Session      zp-api                           ${base_api_url}
    ${token}=           Get Session Token
    &{headers}=         Create Dictionary                content-type=application/json    authorization=${token}
    ${resp}=            Get Request                      zp-api    /customers/${user_id}    headers=${headers}
    [Return]            ${resp}    

# DELETE /customers
Delete Customer
    [Arguments]         ${cpf}
    ${token}=           Get Session Token
    &{headers}=         Create Dictionary    content-type=application/json      authorization=${token}    
    ${resp}=            Delete Request      zp-api               /customers/${cpf}          headers=${headers}
    [Return]            ${resp}
