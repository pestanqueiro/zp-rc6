***Keywords***
### Login
Acesso a página Login
    Go To    ${base_url}

Submeto minhas credenciais
    [Arguments]   ${email}    ${password}  
    Login With    ${email}    ${password}

Devo ver a área logada
    Wait Until Page Contains    Aluguéis    5

Devo ver um toaster com a mensagem
    [Arguments]    ${expected_message}
    Wait Until Element Contains     ${TOASTER_ERROR_P}     ${expected_message}

### Save Customers
Dado que acesso o formulário de cadastro de clientes
    Access Customer Form   

E que eu tenho o seguinte cliente:
    [Arguments]    ${name}    ${cpf}    ${address}   ${phone_number}
    Remove Customer By CPF    ${cpf}
    Set Test Variable         ${name}
    Set Test Variable         ${cpf}
    Set Test Variable         ${address}
    Set Test Variable         ${phone_number}

Mas esse cpf já existe no sistema 
    Insert Customer           ${name}    ${cpf}    ${address}   ${phone_number}

Quando faço a inclusão desse cliente
    Register New Customer     ${name}    ${cpf}    ${address}   ${phone_number}

Então devo ver a notificação:
    [Arguments]    ${expected_notification}
    Wait Until Element Contains    ${TOASTER_SUCCESS}    ${expected_notification}    5

Então devo ver a notificação de erro:
    [Arguments]    ${expected_notification}
    Wait Until Element Contains    ${TOASTER_ERROR}       ${expected_notification}    5

Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatórios
    Wait Until Element Contains    ${LABEL_NAME}       Nome é obrigatório        5
    Wait Until Element Contains    ${LABEL_CPF}        CPF é obrigatório         5
    Wait Until Element Contains    ${LABEL_ADDRESS}    Endereço é obrigatório    5 
    Wait Until Element Contains    ${LABEL_PHONE}      Telefone é obrigatório    5

Então devo ver o texto:    
    [Arguments]    ${expected_text}
    Wait Until Page Contains     ${expected_text}    5

E esse cliente deve ser exibido na lista
    ${cpf_formatado}=         Format CPF    ${cpf}
    Go Back 
    Wait Until Element Is Visible   ${CUSTOMER_LIST}    5
    Table Should Contain            ${CUSTOMER_LIST}    ${cpf_formatado}

### Remove Customer
Dado que eu tenho um cliente indesejado
    [Arguments]               ${name}    ${cpf}    ${address}    ${phone_number}
    Remove Customer By CPF               ${cpf}
    Insert Customer           ${name}    ${cpf}    ${address}    ${phone_number}
    Set Test Variable                    ${cpf}

E acesso a lista de clientes
    Go To Customers

Quando eu removo esse cliente
    ${cpf_formatado}=    Format CPF    ${cpf}    
    Set Test Variable         ${cpf_formatado}
    Go To Customer Details    ${cpf_formatado}
    Click Remove Customer

E esse cliente não deve aparecer na lista
    Wait Until Page Does Not Contain    ${cpf_formatado}

### Save Equipments
Dado que acesso o formulário de cadastro de equipamentos
    Access Equipment Form

E que eu tenho o seguinte equipamento:
    [Arguments]    ${name}    ${value} 
    Remove Equipo By Name     ${name}
    Set Test Variable         ${name}
    Set Test Variable         ${value}

Mas esse equipamento já existe no sistema 
    Insert Equipment          ${name}    ${value}

Quando faço a inclusão desse equipamento
    Register New Equipment     ${name}    ${value}

Então devo ver mensagens informando que os campos do cadastro de equipamentos são obrigatórios
    Wait Until Element Contains    ${LABEL_EQ_NAME}    Nome do equipo é obrigatório        5
    Wait Until Element Contains    ${LABEL_VALUE}      Diária do equipo é obrigatória      5