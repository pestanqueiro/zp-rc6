***Settings***
Documentation    Representação do toaster que mostra as mensagens no sistema

***Variables***
${TOASTER_SUCCESS}    css:div[type=success] strong
${TOASTER_ERROR_P}    css:div[type=Error] p
${TOASTER_ERROR}      css:div[type=Error] strong