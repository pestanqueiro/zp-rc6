***Settings***
Documentation    Representação da página equipamentos com seus elementos e ações

***Variables***
${EQUIPOS_FORM}      css:a[href$=register]    
${LABEL_EQ_NAME}     css:label[for='equipo-name']
${LABEL_VALUE}       css:label[for='daily_price']

***Keywords***
Access Equipment Form 
    Wait Until Element Is Visible    ${NAV_EQUIPOS}        5
    Click Element                    ${NAV_EQUIPOS} 
    Wait Until Element Is Visible    ${EQUIPOS_FORM}       5
    Click Element                    ${EQUIPOS_FORM}
    Wait Until Element Is Visible    css:h1 

Register New Equipment
    [Arguments]    ${name}    ${daily_price} 
    Reload Page
    Input Text      id:equipo-name     ${name}
    Input Text      id:daily_price     ${daily_price}
    Click Element   xpath://button[text()='CADASTRAR']