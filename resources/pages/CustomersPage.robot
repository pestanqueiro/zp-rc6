***Settings***
Documentation    Representação da página clientes com seus elementos e ações

***Variables***
${CUSTOMERS_FORM}      css:a[href$=register]
${LABEL_NAME}          css:label[for='name']        
${LABEL_CPF}           css:label[for='cpf']         
${LABEL_ADDRESS}       css:label[for='address']     
${LABEL_PHONE}         css:label[for='phone_number']
${BUTTON_CADASTRAR}    xpath://button[text()='CADASTRAR']
${CPF_APAGAR}          xpath://td[text()='%s']
${BUTTON_APAGAR}       xpath://button[text()='APAGAR']
${BUTTON_VOLTAR}       xpath://button[text()='VOLTAR']
${CUSTOMER_LIST}       css: table

***Keywords***
Access Customer Form 
    Go To Customers
    Wait Until Element Is Visible    ${CUSTOMERS_FORM}       5
    Click Element                    ${CUSTOMERS_FORM}
    Wait Until Element Is Visible    css:h1 

Register New Customer
    [Arguments]    ${name}    ${cpf}    ${address}    ${phone_number} 
    Reload Page
    Input Text       id:name            ${name}
    Input Text       id:cpf             ${cpf}
    Input Text       id:address         ${address}
    Input Text       id:phone_number    ${phone_number}
    Click Element    ${BUTTON_CADASTRAR}

Go To Customer Details
    [Arguments]    ${cpf_formatado}    
    Wait Until Element Is Visible    ${CPF_APAGAR % '${cpf_formatado}'}    5
    Click Element                    ${CPF_APAGAR % '${cpf_formatado}'}   

Click Remove Customer
    Wait Until Element Is Visible    ${BUTTON_APAGAR}    5
    Click Element                    ${BUTTON_APAGAR}
